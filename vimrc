"     ____               _               _
"    |  _ \  _____   __ | |    __ _ _ __| | _____
"    | | | |/ _ \ \ / / | |   / _` | '__| |/ / __|
"    | |_| |  __/\ V /  | |___ (_| | |  |   <\__ \
"    |____/ \___| \_/___|_____\__,_|_|  |_|\_\___/
"                  |_____|


"----------------------------------------
" General Settings
" ---------------------------------------

" General Settings
set nocompatible
filetype plugin on
syntax on
set scrolloff=10
set guifont=CaskaydiaCove_NF:h12

" Set tab behaviour
set tabstop=4
set shiftwidth=4
set expandtab
set smartindent

" Set line numbering
set relativenumber
set number

"----------------------------------------
" Autocmds
"----------------------------------------

" Centre screen when entering Insert mode
" autocmd InsertEnter * norm zz
"
"
"----------------------------------------
" Key maps
"----------------------------------------

let mapleader=" "
inoremap <nowait> jj <Esc>

" Set y to copy to system clipboard
nnoremap y "*y
vnoremap y "*y


"----------------------------------------
" Plugins
"----------------------------------------


call plug#begin()

"source ~/.config/vim/plugins/fzf.vim
"source C:/Users/larkinc/vimfiles/plugins/lightline.vim
"source ~/.config/vim/plugins/nerdtree.vim
"source ~/.config/vim/plugins/nord-vim.vim
"source ~/.config/vim/plugins/vimwiki.vim
"source ~/.config/vim/plugins/emmet-vim.vim
"source ~/.config/vim/plugins/vim-closetag.vim
"Plug 'itchyny/lightline.vim'
"Plug 'articicestudio/nord-vim'

call plug#end()

colorscheme nord
